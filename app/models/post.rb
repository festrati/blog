class Post < ActiveRecord::Base
    #destroy comment with post
    has_many :comments, dependent: :destroy

    #validations
    validates_presence_of :title
    validates_presence_of :body
end
